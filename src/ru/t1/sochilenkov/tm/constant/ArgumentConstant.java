package ru.t1.sochilenkov.tm.constant;

public final class ArgumentConstant {

    public static final String HELP = "help";

    public static final String VERSION = "version";

    public static final String ABOUT = "about";

    private ArgumentConstant() {
    }

}
